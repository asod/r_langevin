"""Langevin dynamics class."""

import numpy as np

from ase.md.md import MolecularDynamics
from ase.parallel import world, DummyMPI
from ase import units
from ase.geometry import find_mic

from ase.io.trajectory import TrajectoryWriter

class FricDebugger:
    ''' Writes out the friction values in the charge array for the 
        atoms object, so you can easily see if the friction is 
        correctly adjusted. '''
    def __init__(self, tag, atoms, md):
        self.tag = tag
        self.atoms = atoms
        self.md = md
        self.trajwr = TrajectoryWriter(tag + '.traj', 'w')


    def write(self):
        out_atoms = self.atoms.copy()
        out_atoms.constraints = []
        fric = self.md.fr
        if isinstance(fric, float):
            fric = np.zeros(len(out_atoms)) + fric
            
        if len(fric.shape)  == 1:
            out_atoms.set_initial_charges(fric) 
        else:
            out_atoms.set_initial_charges(fric[:, 0])  # weird shape thing again
        self.trajwr.write(out_atoms)



class RLangevin(MolecularDynamics):
    """Langevin (constant N, V, T) molecular dynamics.

       Version where the friction-coupling is scaled on the distance
       from a center definined by the user:

       qmidx: list
           indices of atoms that should never receive friction
       ctidx: list 
           indices of atoms that should always receive the same friction
       mmidx: list
           indices of atoms where the friction coupling schould be distance-
           based
       r_max: float 
           maximum distance before friction starts
       r_buf: float 
           fades in friction linearly from r_max to r_max + r_buf
       apm: int
           atoms per solvent molecule 
       max_friction: float
           maximum friction experienced anywhere. 

       Whatever you put into the 'normal' friction parameter will be OVER-
       WRITTEN with whatever is determined by this mode.

       The final distance-based friction arrays are updated at the start of 
       every step. 
       
    """

    # Helps Asap doing the right thing.  Increment when changing stuff:
    _lgv_version = 4

    def __init__(self, atoms, timestep, temperature=None, friction=None,
                 qmidx=None, ctidx=None, mmidx=None, r_max=None, r_buf=None,
                 apm=3, max_friction=None,
                 fixcm=True, *, temperature_K=None, trajectory=None,
                 logfile=None, loginterval=1, communicator=world,
                 rng=None, append_trajectory=False):
        """
        Parameters:

        atoms: Atoms object
            The list of atoms.

        timestep: float
            The time step in ASE time units.

        temperature: float (deprecated)
            The desired temperature, in electron volt.

        temperature_K: float
            The desired temperature, in Kelvin.

        friction: float
            A friction coefficient, typically 1e-4 to 1e-2.

        fixcm: bool (optional)
            If True, the position and momentum of the center of mass is
            kept unperturbed.  Default: True.

        rng: RNG object (optional)
            Random number generator, by default numpy.random.  Must have a
            standard_normal method matching the signature of
            numpy.random.standard_normal.

        logfile: file object or str (optional)
            If *logfile* is a string, a file with that name will be opened.
            Use '-' for stdout.

        trajectory: Trajectory object or str (optional)
            Attach trajectory object.  If *trajectory* is a string a
            Trajectory will be constructed.  Use *None* (the default) for no
            trajectory.

        communicator: MPI communicator (optional)
            Communicator used to distribute random numbers to all tasks.
            Default: ase.parallel.world. Set to None to disable communication.

        append_trajectory: bool (optional)
            Defaults to False, which causes the trajectory file to be
            overwritten each time the dynamics is restarted from scratch.
            If True, the new structures are appended to the trajectory
            file instead.

        The temperature and friction are normally scalars, but in principle one
        quantity per atom could be specified by giving an array.

        RATTLE constraints can be used with these propagators, see:
        E. V.-Eijnden, and G. Ciccotti, Chem. Phys. Lett. 429, 310 (2006)

        The propagator is Equation 23 (Eq. 39 if RATTLE constraints are used)
        of the above reference.  That reference also contains another
        propagator in Eq. 21/34; but that propagator is not quasi-symplectic
        and gives a systematic offset in the temperature at large time steps.
        """
        if friction is None:
            raise TypeError("Missing 'friction' argument.")
        self.fr = friction
        self.temp = temperature  #units.kB * self._process_temperature(temperature, 
                    #                                     temperature_K, 'eV')
        self.fix_com = fixcm
        self.qmidx = qmidx
        self.ctidx = ctidx
        self.mmidx = mmidx
        self.r_max = r_max
        self.r_buf = r_buf
        self.apm = apm
        self.max_friction = max_friction
        if communicator is None:
            communicator = DummyMPI()
        self.communicator = communicator
        if rng is None:
            self.rng = np.random
        else:
            self.rng = rng
        MolecularDynamics.__init__(self, atoms, timestep, trajectory,
                                   logfile, loginterval,
                                   append_trajectory=append_trajectory)
        self.updatevars()

    def todict(self):
        d = MolecularDynamics.todict(self)
        d.update({'temperature_K': self.temp / units.kB,
                  'friction': self.fr,
                  'fixcm': self.fix_com})
        return d

    def set_temperature(self, temperature=None, temperature_K=None):
        self.temp = units.kB * self._process_temperature(temperature,
                                                         temperature_K, 'eV')
        self.updatevars()

    def set_friction(self, friction):
        self.fr = friction
        self.updatevars()

    def set_timestep(self, timestep):
        self.dt = timestep
        self.updatevars()

    def updatevars(self):
        dt = self.dt
        T = self.temp
        fr = self.fr
        masses = self.masses
        sigma = np.sqrt(2 * T * fr / masses)

        self.c1 = dt / 2. - dt * dt * fr / 8.
        self.c2 = dt * fr / 2 - dt * dt * fr * fr / 8.
        self.c3 = np.sqrt(dt) * sigma / 2. - dt**1.5 * fr * sigma / 8.
        self.c5 = dt**1.5 * sigma / (2 * np.sqrt(3))
        self.c4 = fr / 2. * self.c5

    
    def update_friction(self):
        atoms = self.atoms.copy()
        atoms.constraints = []
        qmidx = self.qmidx
        ctidx = self.ctidx
        mmidx = self.mmidx
        r_max = self.r_max
        r_buf = self.r_buf

        fric = np.zeros(len(atoms))
        nofric_mask = np.zeros(len(atoms), bool)
        nofric_mask[qmidx] = True
        mmpos = atoms[mmidx].positions.reshape((-1, self.apm, 3))  # shape per mol
        qmcenter = atoms[qmidx].positions.mean(axis=0)
        _, d = find_mic(mmpos[:, 0, :] - qmcenter, atoms.cell, atoms.pbc)  # per  mol
        d = np.repeat(d, 3)  # back up to all atom
        all_d = np.zeros(len(atoms)) 
        all_d[mmidx] = d 

        fac = (all_d - r_max) / r_buf
        fac[all_d > r_max + r_buf] = 1
        fac[all_d < r_max] = 0

        fric = fac * self.max_friction
        fric[all_d > r_max + r_buf] = self.max_friction
        if ctidx:
            fric[ctidx] = self.max_friction

        return fric


    def step(self, forces=None):
        atoms = self.atoms
        natoms = len(atoms)

        if forces is None:
            forces = atoms.get_forces()

        if self.qmidx:  #  Do the distance-based friction stuff. 
            dist_fric = self.update_friction()
            df = np.repeat(dist_fric[:, np.newaxis], 3, axis=1)  # weird dims 
            self.set_friction(df)

        # This velocity as well as xi, eta and a few other variables are stored
        # as attributes, so Asap can do its magic when atoms migrate between
        # processors.
        self.v = atoms.get_velocities()

        self.xi = self.rng.standard_normal(size=(natoms, 3))
        self.eta = self.rng.standard_normal(size=(natoms, 3))

        # When holonomic constraints for rigid linear triatomic molecules are
        # present, ask the constraints to redistribute xi and eta within each
        # triple defined in the constraints. This is needed to achieve the
        # correct target temperature.
        for constraint in self.atoms.constraints:
            if hasattr(constraint, 'redistribute_forces_md'):
                constraint.redistribute_forces_md(atoms, self.xi, rand=True)
                constraint.redistribute_forces_md(atoms, self.eta, rand=True)

        self.communicator.broadcast(self.xi, 0)
        self.communicator.broadcast(self.eta, 0)

        # First halfstep in the velocity.
        self.v += (self.c1 * forces / self.masses - self.c2 * self.v +
                   self.c3 * self.xi - self.c4 * self.eta)

        # Full step in positions
        x = atoms.get_positions()
        if self.fix_com:
            old_com = atoms.get_center_of_mass()
        # Step: x^n -> x^(n+1) - this applies constraints if any.
        atoms.set_positions(x + self.dt * self.v + self.c5 * self.eta)

        # The master branch has a new set_com, which niflheim does not have yet
        #if self.fix_com:
        #    atoms.set_center_of_mass(old_com)
        if self.fix_com:
            new_com = atoms.get_center_of_mass()
            d = old_com - new_com
            atoms.set_positions(atoms.get_positions() + d)

        # recalc velocities after RATTLE constraints are applied
        self.v = (self.atoms.get_positions() - x -
                  self.c5 * self.eta) / self.dt
        forces = atoms.get_forces(md=True)

        # Update the velocities
        self.v += (self.c1 * forces / self.masses - self.c2 * self.v +
                   self.c3 * self.xi - self.c4 * self.eta)
        
        if self.fix_com:  # subtract center of mass vel
            self.v -= self._get_com_velocity()


        # Second part of RATTLE taken care of here
        atoms.set_momenta(self.v * self.masses)

        return forces

    def _get_com_velocity(self):
        """Return the center of mass velocity.

        Internal use only.  This function can be reimplemented by Asap.
        """
        return np.dot(self.masses.flatten(), self.v) / self.masses.sum()

